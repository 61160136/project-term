import 'dart:html';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:flutter/material.dart';

class ProfileWidget extends StatefulWidget {
  var userId;
  ProfileWidget({Key? key, required this.userId}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState(this.userId);
}

class _ProfileWidgetState extends State<ProfileWidget> {
  FirebaseAuth auth = FirebaseAuth.instance;
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  final Stream<QuerySnapshot> usersStream =
      FirebaseFirestore.instance.collection('users').snapshots();
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  var userId;
  _ProfileWidgetState(this.userId);
  int age = 0;
  String gender = ' ';
  int height = 0;
  int weight = 0;
  double bmi = 0;

  void initState() {
    super.initState();

    if (this.userId.isNotEmpty) {
      users.doc(this.userId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          age = data['age'];
          bmi = data['bmi'];
          gender = data['gender'];
          height = data['height'];
          weight = data['weight'];

          ageController.text = age.toString();
          genderController.text = gender;
          heightController.text = height.toString();
          weightController.text = weight.toString();
        }
      });
    }
  }

  TextEditingController ageController = TextEditingController();
  TextEditingController genderController = TextEditingController();
  TextEditingController heightController = TextEditingController();
  TextEditingController weightController = TextEditingController();

  Widget _buildPopupDialog(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    @override
    Future<void> updateUser() {
      return users
          .doc(this.userId)
          .update({
            'age': this.age,
            'bmi': this.bmi.toStringAsFixed(2),
            'gender': this.gender,
            'height': this.height,
            'weight': this.weight
          })
          .then((value) => print("User Updated"))
          .catchError((error) => print("Failed to update user: $error"));
    }

    return new AlertDialog(
      title: const Text(''),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextField(
            decoration: InputDecoration(
              // icon: Icon(Icons.account_circle),
              labelText: 'อายุ',
            ),
            controller: ageController,
            onChanged: (value) {
              setState(() {
                age = int.tryParse(value)!;
              });
            },
          ),
          TextField(
            decoration: InputDecoration(
              // icon: Icon(Icons.lock),
              labelText: 'เพศ',
            ),
            controller: genderController,
            onChanged: (value) {
              setState(() {
                gender = value;
              });
            },
          ),
          TextField(
            decoration: InputDecoration(
              // icon: Icon(Icons.lock),
              labelText: 'ส่วนสูง',
            ),
            controller: heightController,
            onChanged: (value) {
              setState(() {
                height = int.tryParse(value)!;
              });
            },
          ),
          TextField(
            decoration: InputDecoration(
              // icon: Icon(Icons.lock),
              labelText: 'น้ำหนัก',
            ),
            controller: weightController,
            onChanged: (value) {
              setState(() {
                weight = int.tryParse(value)!;
              });
            },
          ),
        ],
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            bmi = (weight / pow(height / 100, 2));
            updateUser();
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Submit'),
        ),
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Close'),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green.shade200,
          title: Text("PROFILE", style: TextStyle(color: Colors.white)),
          centerTitle: true,
          actions: [
          
            IconButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => _buildPopupDialog(context),
                );
              },
              icon: Icon(Icons.edit),
            ),
              IconButton(
              onPressed: () async {
                FirebaseAuth.instance.signOut();
              },
              icon: Icon(IconData(0xe3b3, fontFamily: 'MaterialIcons')),
            ),
          ],
        ),
        body: GetUserName(this.userId));
  }
}

class GetUserName extends StatelessWidget {
  final String documentId;

  GetUserName(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference users = FirebaseFirestore.instance.collection('users');
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;

          // return Text("Full Name: ${data['height']} ${data['bmi']}");
          return Column(
            children: <Widget>[
              Container(
                  // decoration: BoxDecoration(
                  //   gradient: LinearGradient(
                  //     begin: Alignment.topCenter,
                  //     end: Alignment.bottomCenter,
                  //     colors: [Colors.redAccent, Colors.pinkAccent]
                  //   )
                  // ),
                  child: Container(
                // width: 200.0,
                // height: 40.0,
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 10.0,
                      ),
                      // CircleAvatar(
                      //   backgroundImage: NetworkImage(
                      //     "https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg.jpg",
                      //   ),
                      //   radius: 50.0,
                      // ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        "",
                        style: TextStyle(
                          fontSize: 22.0,
                          color: Colors.black,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),

                      Card(
                        margin: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 5.0),
                        clipBehavior: Clip.antiAlias,
                        color: Colors.white,
                        elevation: 5.0,
                        child: ListTile(
                          // leading: FlutterLogo(),
                          title: Text('อายุ'),
                          trailing: Text("${data['age']}"),
                        ),
                      ),
                      Card(
                        margin: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 5.0),
                        clipBehavior: Clip.antiAlias,
                        color: Colors.white,
                        elevation: 5.0,
                        child: ListTile(
                          // leading: FlutterLogo(),
                          title: Text('เพศ'),
                          trailing: Text("${data['gender']}"),
                        ),
                      ),
                      Card(
                        margin: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 5.0),
                        clipBehavior: Clip.antiAlias,
                        color: Colors.white,
                        elevation: 5.0,
                        child: ListTile(
                          // leading: FlutterLogo(),
                          title: Text('ส่วนสูง'),
                          trailing: Text("${data['height']}"),
                        ),
                      ),
                      Card(
                        margin: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 5.0),
                        clipBehavior: Clip.antiAlias,
                        color: Colors.white,
                        elevation: 5.0,
                        child: ListTile(
                          // leading: FlutterLogo(),
                          title: Text('น้ำหนัก'),
                          trailing: Text("${data['weight']}"),
                        ),
                      ),
                    ],
                  ),
                ),
              )),
            ],
          );
        }

        return Text("loading");
      },
    );
  }
}
