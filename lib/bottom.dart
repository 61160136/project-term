import 'package:bmi_calculator/age_height_weight.dart';
import 'package:bmi_calculator/body_mass_index.dart';
import 'package:bmi_calculator/gender.dart';
import 'package:bmi_calculator/history.dart';
import 'package:bmi_calculator/profile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Bottom extends StatefulWidget {
  var userId;
  Bottom({Key? key, required this.userId}) : super(key: key);
  @override
  _BottomState createState() => _BottomState(this.userId);
}

class _BottomState extends State<Bottom> {
  var userId;
  _BottomState(this.userId);
  int _selectedPage = 0;
var _pageOptions;
  @override
  void initState() {
    super.initState();
     _pageOptions = [
      Body_Mass_IndexWidget(userId: userId),
      StoryWidget(userId: userId),
      ProfileWidget(userId: userId)
    ];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // title: 'Buttom Navigation Bar Demo',
      // theme: ThemeData(
      //   primarySwatch: Colors.orange,
      // ),
      home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Bootom Navigation Bar'),
        // ),
        body: _pageOptions[_selectedPage],
        bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Colors.green.shade200,
            selectedItemColor: Colors.black,
            unselectedItemColor: Colors.white,
            currentIndex: _selectedPage,
            onTap: (int index) {
              setState(() {
                _selectedPage = index;
              });
            },
            items: [
              BottomNavigationBarItem(
                  icon: Icon(IconData(0xeeb2, fontFamily: 'MaterialIcons')),
                  label: 'BMI'),
              BottomNavigationBarItem(icon: Icon(Icons.work), label: 'น้ำหนัก'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person), label: 'Profile'),
            ]),
      ),
    );
  }
}

// class GetUserName extends StatelessWidget {
//   final String documentId;

//   GetUserName(this.documentId);

//   @override
//   Widget build(BuildContext context) {
//     CollectionReference users = FirebaseFirestore.instance.collection('users');

//     return FutureBuilder<DocumentSnapshot>(
//       future: users.doc(documentId).get(),
//       builder:
//           (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
//         if (snapshot.hasError) {
//           return Text("Something went wrong");
//         }
 
//         if (snapshot.hasData && !snapshot.data!.exists) {
//           return Text("Document does not exist");
//         }

//         if (snapshot.connectionState == ConnectionState.done) {
//           Map<String, dynamic> data =
//               snapshot.data!.data() as Map<String, dynamic>;

//           return Text(snapshot.data.docs[0].referece.id.toString());
//         }

//         return Text("loading");
//       },
//     );
//   }
// }



