import 'dart:html';

import 'package:bmi_calculator/age_height_weight.dart';
import 'package:bmi_calculator/bottom.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GenderWidget extends StatefulWidget {
  var userId;
  GenderWidget({Key? key, required this.userId}) : super(key: key);

  @override
  _GenderWidgetState createState() => _GenderWidgetState(this.userId);
}

class _GenderWidgetState extends State<GenderWidget> {
  String gender = ' ';
  var userId;

  _GenderWidgetState(this.userId);
  Future<void> _loadGender() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      gender = prefs.getString('gender') ?? '';
    });
  }

  Future<void> _saveGender() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('gender', gender);
    });
  }

  CollectionReference users = FirebaseFirestore.instance.collection('users');
  late Stream<QuerySnapshot> _usersStream;
  @override
  void initState() {
    super.initState();
    _usersStream = FirebaseFirestore.instance
        .collection('users')
        .where('userId', isEqualTo: userId)
        .snapshots();
  }

  Future<void> addUser() {
    return users
        .add({'userId': this.userId})
        .then((value) => value.id)
        .catchError((error) => print("Failed to add user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    final Stream<QuerySnapshot> _notesStream =
        FirebaseFirestore.instance.collection('users').snapshots();

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green.shade200,
          title: Text('GENDER'),
          centerTitle: true,
        ),
        body: Column(
          children: [
            StreamBuilder<QuerySnapshot>(
              stream: _usersStream,
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) {
                  return Text('Something went wrong');
                }

                if (snapshot.connectionState == ConnectionState.waiting) {
                  addUser();
                  print(userId + "111");

                  return Text("Loading");
                }
                  // if(snapshot.data == null){

                  //   print('------');
                  // }
                  return Column(
                    children:
                        snapshot.data!.docs.map((DocumentSnapshot document) {
                      Map<String, dynamic> data =
                          document.data()! as Map<String, dynamic>;

                      if (data['userId'] == userId) {
                        
                        print(userId + '------11');

                        return new Center(
                            child: Column(children: <Widget>[
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(50.0),
                            margin: EdgeInsets.only(top: 100.0),
                            child: new Text(
                              "What's your gender ?",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          new Center(
                              child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              ElevatedButton(
                                onPressed: () async {
                                  setState(() {
                                    gender = 'M';
                                  });
                                  _saveGender();
                                  await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            Age_Height_WeightWidget(
                                                userId: document.id)),
                                  );
                                },
                                child: Icon(
                                  IconData(0xe3c5, fontFamily: 'MaterialIcons'),
                                  color: Colors.white,
                                  size: 60.0,
                                ),
                                style: ElevatedButton.styleFrom(
                                    shape: CircleBorder(),
                                    primary: Colors.green.shade200),
                              ),
                              ElevatedButton(
                                onPressed: () async {
                                  setState(() {
                                    gender = 'F';
                                  });

                                  _saveGender();
                                  await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            Age_Height_WeightWidget(
                                                userId: document.id)),
                                  );
                                },
                                child: Icon(
                                  IconData(57953, fontFamily: 'MaterialIcons'),
                                  color: Colors.white,
                                  size: 60.0,
                                ),
                                style: ElevatedButton.styleFrom(
                                    shape: CircleBorder(),
                                    primary: Colors.green.shade200),
                              ),
                            ],
                          )),
                        ]));
                      } else {
                        return Center(
                            child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                   SizedBox(
                        height: 60.0,
                      ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.green.shade200,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 50, vertical: 10),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20))),
                                child: Text(
                                  'Get Started',
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),

                                onPressed: () async {
                                  await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            Bottom(userId: document.id)),
                                  );
                                },
                                // child: Text('Sign In'),
                              ),
                            ]));
                      }
                    }).toList(),
                  );
                
              },
            ),
          ],
        )
        
        );
  }
}