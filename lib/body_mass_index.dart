import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Body_Mass_IndexWidget extends StatefulWidget {
  
  var userId;
  Body_Mass_IndexWidget({Key? key, required this.userId}) : super(key: key);

  @override
  _Body_Mass_IndexWidgetState createState() => _Body_Mass_IndexWidgetState(this.userId);
}


class _Body_Mass_IndexWidgetState extends State<Body_Mass_IndexWidget> {
  int age = 0;
  int height = 0;
  int weight = 0;
  double bmi = 0;
  var userId;
  String bmi1 = '';
  _Body_Mass_IndexWidgetState(this.userId);

// DocumentReference id = FirebaseFirestore.instance.collection('users').doc();
  @override
  void initState() {
    super.initState();
    _loadAge_Height_Weight();
  }

  String _message = 'Please enter your height an weight';


  Future<void> _loadAge_Height_Weight() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      age = prefs.getInt('age') ?? 0;
      height = prefs.getInt('height') ?? 0;
      weight = prefs.getInt('weight') ?? 0;
    });

    bmi = weight / pow(height / 100, 2);
    if (bmi < 16) {
      _message = "ผอมรุนแรงมาก";
    } else if (bmi < 16.9) {
      _message = 'ผอมรุนแรง';
    } else if (bmi < 18.4) {
      _message = 'ผอม';
    } else if (bmi < 24.9) {
      _message = 'ปกติ';
    } else if (bmi < 29.9) {
      _message = 'น้ำหนักเกิน';
    } else if (bmi < 34.9) {
      _message = 'อ้วนระดับ 1';
    } else if (bmi < 39.9) {
      _message = 'อ้วนระดับ 2';
    } else {
      _message = 'อ้วนระดับ 3';
    }
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('height');
    prefs.remove('weight');
    prefs.remove('age');
    
    await _loadAge_Height_Weight();
  }
  
  // Color color = Colors.grey.shade300;
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      // backgroundColor: Colors.green.shade200,
      appBar: AppBar(
       backgroundColor: Colors.green.shade200,
        title: Text('Body Mass Index'),
        // elevation: 0,
        centerTitle: true,
      ),
      body: GetUserName(this.userId)
    );
  }
}

class GetUserName extends StatelessWidget {
  final String documentId;

  GetUserName(this.documentId);
  String _message = ' ';
    String bmi1 = '';
Color color = Colors.grey.shade300;
  @override
  Widget build(BuildContext context) {
     BoxDecoration myBoxDecoration() {
     var bmi = double.parse(bmi1);
      
     if (bmi < 16) {
       color = Colors.blue.shade900;
       _message = "ผอมรุนแรงมาก";
    } else if (bmi < 16.9) {
      color = Colors.blue.shade600;
      _message = "ผอมรุนแรง";
    } else if (bmi < 18.4) {
      color = Colors.lightBlueAccent;
      _message = "ผอม";
    } else if (bmi < 24.9) {
       color = Colors.lightGreen.shade600;
        _message = 'ปกติ';
    } else if (bmi < 29.9) {
       color = Colors.amber;
       _message = 'น้ำหนักเกิน';
    } else if (bmi < 34.9) {
      color = Colors.amber.shade900;
      _message = 'อ้วนระดับ 1';
    } else if (bmi < 39.9) {
      color = Colors.deepOrangeAccent.shade400;
      _message = 'อ้วนระดับ 2';
    } else {
      color = Colors.deepOrangeAccent.shade700;
      _message = 'อ้วนระดับ 3';
    }
      
      return BoxDecoration(
        border: Border.all(width: 2),
        shape: BoxShape.circle,
        color: color,
      );
    }

 

    CollectionReference users = FirebaseFirestore.instance.collection('users');

    return FutureBuilder<DocumentSnapshot>(
      future: users.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.hasData && !snapshot.data!.exists) {
          return Text("Document does not exist");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;
              
              bmi1 = data['bmi'];
         
          return Center(
        child: ListView(
          children: <Widget>[
              Card(
                      margin: EdgeInsets.symmetric(horizontal: 20.0,vertical: 5.0),
                      clipBehavior: Clip.antiAlias,
                      color: Colors.white,
                      elevation: 5.0,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 22.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Column(

                                children: <Widget>[
                                  Text(
                                    "Age",
                                    style: TextStyle(
                                      color: Colors.redAccent,
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Text(
                                    "${data['age']}",
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.pinkAccent,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(

                                children: <Widget>[
                                  Text(
                                    "Height",
                                    style: TextStyle(
                                      color: Colors.redAccent,
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Text(
                                    "${data['age'] } CM",
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.pinkAccent,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(

                                children: <Widget>[
                                  Text(
                                    "Weight",
                                    style: TextStyle(
                                      color: Colors.redAccent,
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Text(
                                    "${data['weight']} KG",
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.pinkAccent,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            
                          ],
                          
                        ),
                        
                      ),
                      
                    ),
               
              
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("${data['bmi']}",
                            style: TextStyle(
                              fontSize: 24.0,
                            )
                            
                            ),
                            
                      ],
                      
                    ),
                    
                    width: 100,
                    height: 100,
                    
                    decoration: myBoxDecoration(),
                  ),
                  // Padding(
                  //   padding: const EdgeInsets.all(16.0),
                  //   child: Text('BMI : ${bmi.toStringAsFixed(2)}',
                  //       style: TextStyle(
                  //           fontWeight: (bmi > 30)
                  //               ? FontWeight.bold
                  //               : FontWeight.normal,
                  //           fontSize: 24.0,
                  //           color: (bmi > 30)
                  //               ? Colors.red.withOpacity(0.6)
                  //               : Colors.black.withOpacity(0.6))),
                  // ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Text(
                      _message,
                      style: TextStyle(color: Colors.black.withOpacity(0.6), fontWeight: FontWeight.w200, fontSize: 16.0),
                      textAlign: TextAlign.center,
                    ),
                  ),
                SizedBox(
                    height: 20,
                  ),
              
              
            
            Container(),
           
            ListTile(
              leading: Icon(
                IconData(57699, fontFamily: 'MaterialIcons'),
                color: Colors.blue.shade900,
              ),
              title: Text('ผอมรุนแรงมาก'),
              trailing: const Text("< 16",
              // style: TextStyle(color: Colors.green),
              )
              
            ,
            ),
            ListTile(
              leading: Icon(
                IconData(57699, fontFamily: 'MaterialIcons'),
                color: Colors.blue.shade600,
              ),
              title: Text('ผอมรุนแรง'),
              trailing: const Text("16.0 - 16.9"),
            ),
            ListTile(
              leading: Icon(
                IconData(57699, fontFamily: 'MaterialIcons'),
                color: Colors.lightBlueAccent,
              ),
              title: Text('ผอม'),
              trailing: const Text("17.0 - 18.4"),
            ),
            ListTile(
              leading: Icon(
                IconData(57699, fontFamily: 'MaterialIcons'),
                color: Colors.lightGreen.shade600,
              ),
              title: Text('ปกติ'),
              trailing: const Text("18.5 - 24.9"),
            ),
            ListTile(
              leading: Icon(
                IconData(57699, fontFamily: 'MaterialIcons'),
                color: Colors.amber,
              ),
              title: Text('น้ำหนักเกิน'),
              trailing: const Text("25.0 - 29.9"),
            ),
            ListTile(
              leading: Icon(
                IconData(57699, fontFamily: 'MaterialIcons'),
                color: Colors.amber.shade900,
              ),
              title: Text('อ้วนระดับ 1'),
              trailing: const Text("30.0 - 34.9"),
            ),
            ListTile(
              leading: Icon(
                IconData(57699, fontFamily: 'MaterialIcons'),
                color: Colors.deepOrangeAccent.shade400,
              ),
              title: Text('อ้วนระดับ 2'),
              trailing: const Text("35.0 - 39.9"),
            ),
            ListTile(
              leading: Icon(
                IconData(57699, fontFamily: 'MaterialIcons'),
                color: Colors.deepOrangeAccent.shade700,
              ),
              title: Text('อ้วนระดับ 3'),
              trailing: const Text(">= 40.0"),
            ),
                
          ],
        ),
      );
        }

        return Text("loading");
      },
    );
  }
}
