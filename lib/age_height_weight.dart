import 'dart:html';
import 'dart:math';

import 'package:bmi_calculator/body_mass_index.dart';
import 'package:bmi_calculator/bottom.dart';
import 'package:bmi_calculator/profile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:ui';

class Age_Height_WeightWidget extends StatefulWidget {
  var userId;
  Age_Height_WeightWidget({Key? key, required this.userId}) : super(key: key);

  @override
  _Age_Height_WeightWidgetState createState() =>
      _Age_Height_WeightWidgetState(this.userId);
}

class _Age_Height_WeightWidgetState extends State<Age_Height_WeightWidget> {
  final _formKey = GlobalKey<FormState>();
  var userId;
  _Age_Height_WeightWidgetState(this.userId);
  String id = '';
  String id1 = '';
  int age = 0;
  String gender = ' ';
  int height = 0;
  int weight = 0;
  double bmi = 0;
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  final _ageController = TextEditingController();
  final _heightController = TextEditingController();
  final _weightController = TextEditingController();
  final _bmiController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadAge_Height_Weight();
    _loadGender();
  }

  Future<void> _loadAge_Height_Weight() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      age = prefs.getInt('age') ?? 0;
      _ageController.text = '$age';
      height = prefs.getInt('height') ?? 0;
      _heightController.text = '$height';
      weight = prefs.getInt('weight') ?? 0;
      _weightController.text = '$weight';
    });
  }

  Future<void> _saveAge_Height_Weight() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setInt('age', age);
      prefs.setInt('height', height);
      prefs.setInt('weight', weight);
    });
  }

  Future<void> _loadGender() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      gender = prefs.getString('gender') ?? '';
    });
  }

  Future<void> _saveGender() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('gender', gender);
    });
  }

  // Future<void> addUser() {
  //   return users
  //       .add({
  //         'age': this.age,
  //         'bmi': this.bmi.toStringAsFixed(2),
  //         'gender': this.gender,
  //         'height': this.height,
  //         'weight': this.weight,
  //         'userId': this.userId
  //       })
  //       .then((value) => value.id)
  //       .catchError((error) => print("Failed to add user: $error"));
  // }

  Future<void> updateUser() {
    return users
        .doc(this.userId)
        .update({
          'age': this.age,
          'bmi': this.bmi.toStringAsFixed(2),
          'gender': this.gender,
          'height': this.height,
          'weight': this.weight
        })
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green.shade200,
      appBar: AppBar(
        title: Text('Age Height Weight'),
        centerTitle: true,
        //backgroundColor: Colors.transparent,
        backgroundColor: Colors.green.shade200,
        elevation: 0,
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Container(
            width: 320,
            child: Card(
              color: Colors.white,
              elevation: 10,
              // padding: EdgeInsets.all(10.0),
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                        alignment: Alignment.topLeft,
                        // padding: EdgeInsets.all(10),
                        child: Text(
                          "What's your age ?",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        )),
                    TextFormField(
                      // style: TextStyle(
                      //     fontSize: 16.0, height: 2.0, color: Colors.black),
                      autofocus: true,
                      controller: _ageController,
                      validator: (value) {
                        var num = int.tryParse(value!);
                        if (num == null || num < 0) {
                          return 'Please input age ';
                        }
                        return null;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          // border: OutlineInputBorder(),
                          ),
                      // style: TextStyle(fontSize: 18, color: Colors.black),
                      onChanged: (value) {
                        setState(() {
                          age = int.tryParse(value)!;
                        });
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        alignment: Alignment.topLeft,
                        // padding: EdgeInsets.all(10),
                        child: Text(
                          "What's your height ?",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        )),
                    TextFormField(
                      controller: _heightController,
                      validator: (value) {
                        var num = int.tryParse(value!);
                        if (num == null || num < 0) {
                          return 'Please input height';
                        }
                        return null;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(),
                      onChanged: (value) {
                        setState(() {
                          height = int.tryParse(value)!;
                        });
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        alignment: Alignment.topLeft,
                        // padding: EdgeInsets.all(10),
                        child: Text(
                          "What's your weight ?",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        )),
                    TextFormField(
                      controller: _weightController,
                      validator: (value) {
                        var num = int.tryParse(value!);
                        if (num == null || num < 0) {
                          return 'Please input weight';
                        }
                        return null;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(),
                      onChanged: (value) {
                        setState(() {
                          weight = int.tryParse(value)!;
                        });
                      },
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    ElevatedButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          bmi = weight / pow(height / 100, 2);
                          id1 = users.doc().toString();
                          updateUser();
                          _saveAge_Height_Weight();

                          await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Bottom(userId: userId)),
                          );
                          // await Navigator.push(
                          //   context,
                          //   MaterialPageRoute(
                          //       builder: (context) => Body_Mass_IndexWidget(userId: '')),

                          // );
                        }
                      },
                      child: const Text('NEXT'),
                      style: TextButton.styleFrom(
                          primary: Colors.white,
                          backgroundColor: Colors.blue.shade200,
                          textStyle: TextStyle(fontSize: 16)),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
