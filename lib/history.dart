import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

class StoryWidget extends StatefulWidget {
  var userId;
  StoryWidget({Key? key, required this.userId}) : super(key: key);

  @override
  _StoryWidgetState createState() => _StoryWidgetState(this.userId);
}

class _StoryWidgetState extends State<StoryWidget> {
  List<WeightSave> weightSaves = [];
  var userId;
  _StoryWidgetState(this.userId);
   CollectionReference historys = FirebaseFirestore.instance.collection('historys');

  void _addWeightEntry() {
    setState(() {
      weightSaves.add(new WeightSave(
        
          new DateTime.now(), 
          new Random().nextInt(100).toInt()));
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green.shade200,
          title: Text("History", style: TextStyle(color: Colors.white)),
          centerTitle: true,
          actions: [
            IconButton(
              onPressed: ()  {
                // await Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => MyLoginPage()),
                // );
                _addWeightEntry();
       
              },
              icon: Icon(IconData(0xe047, fontFamily: 'MaterialIcons')),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.more_vert),
            ),
          ],
        ),
            body: ListView(
        children: weightSaves.map((WeightSave weightSave) {
          //calculating difference
          
          int difference = weightSaves.first == weightSave
              ? 0
              : weightSave.weight -
                  weightSaves[weightSaves.indexOf(weightSave) - 1].weight;
          return WeightListItem(weightSave, difference);
        }).toList(),
      ),
        );
  }
}

class WeightSave {
  DateTime dateTime;
  int weight;

  WeightSave(this.dateTime, this.weight);
}

class WeightListItem extends StatelessWidget {
  final WeightSave weightSave;
  final int weightDifference;

  WeightListItem(this.weightSave, this.weightDifference);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: [
          Expanded(
            child: Column(
              children: [
                Text(
                  DateFormat.yMMMMd().format(weightSave.dateTime),
                  textScaleFactor: 0.9,
                  textAlign: TextAlign.left,
                ),
                Text(
                  DateFormat.EEEE().format(weightSave.dateTime),
                  textScaleFactor: 0.8,
                  textAlign: TextAlign.right,
                  style: TextStyle(color: Colors.grey),
                ),
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
          ),
          Expanded(
            child: Text(
              weightSave.weight.toString(),
              textScaleFactor: 2.0,
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            child: Text(
              weightDifference.toString(),
              textScaleFactor: 1.6,
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }
}


// class GetUserName extends StatelessWidget {
//   final String documentId;
  
//   GetUserName(this.documentId);

//   @override
//   Widget build(BuildContext context) {
//     CollectionReference users = FirebaseFirestore.instance.collection('users');
//     return FutureBuilder<DocumentSnapshot>(
//       future: users.doc(documentId).get(),
//       builder:
//           (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
//         if (snapshot.hasError) {
//           return Text("Something went wrong");
//         }

//         if (snapshot.hasData && !snapshot.data!.exists) {
//           return Text("Document does not exist");
//         }

//         if (snapshot.connectionState == ConnectionState.done) {
//           Map<String, dynamic> data =
//               snapshot.data!.data() as Map<String, dynamic>;
            
//              return Text('${data['weight']}') ;

//           // return Text("Full Name: ${data['height']} ${data['bmi']}");
          
//         }

//         return Text("loading");
//       },
//     );
//   }
// }